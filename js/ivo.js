(function() {
    /* eslint no-undef:0, semi:2 */
    /* eslint guard-for-in:0, semi:2*/
    if (typeof ivo === "undefined") {
      window.ivo = {};
    }
    var eventQueue = [];
    document.addEventListener('DOMContentLoaded', function () {
      eventQueue.forEach(function (fn) {
        fn();
      });
    }, false);
    ivo = function (arg) {
      var htmlEls;
      if (arg instanceof Function) {
        eventQueue.push(arg);
        return document;
      } else if(arg instanceof Object) {
        return new vue([arg]);
      } else {
        if (arg instanceof HTMLElement) {
          htmlEls = [arg]
        } else {
          matches = arg.match(/^<(\w+)>ivo/);
          if (matches) {
            htmlEls = [document.createElement(matches[1])];
          } else {
            htmlEls = Array.prototype.slice.call(document.querySelectorAll(arg));
          }
        }
        return new vue(htmlEls);
      }
    };
    vue = function (elements) {
      this.elements = elements;
      return this;
    };
    vue.prototype.odometer = function (o) {
      let t=this;
      this.onStart=function(){
        this.start=o.start;
        this.finish=o.finish;
        this.time=o.time;
        clearInterval(this.time);
        this.timer =setInterval(() => {
          if(t.start<t.finish){
            t.text(t.start.toFixed(1)+o.textAux);
            t.start+=o.step;
          }else{
            t.text(t.finish+o.textAux);
            clearInterval(this.timer);
          }
        }, t.time);
        
      }
      return this;
    };
    vue.prototype.text = function(string) {
        if (typeof string !== 'undefined') {
          this.elements.forEach(function (el) {
            el.innerText = string;
          });
          return this;
        } else {
          text = "";
          this.elements.forEach(function(element){
            text += element.innerText;
          });
          return text;
        }
      };
    ivo.info=function(options){
        var defaults = {
            title: "",
            icon: ""
        };
        ivo.extend(defaults, options);
        var titleText = defaults.title + " ";
        var self = this;
        //icono pagina//
        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = defaults.icon;
        document.getElementsByTagName('head')[0].appendChild(link);
        //titulo rotativo//
        this.titleMarquee = function () {
            titleText = titleText.substring(1, titleText.length) + titleText.substring(0, 1);
            document.title = titleText;
            setTimeout(function () {
                self.titleMarquee();
            }, 450);
        }
        this.titleMarquee();
        var style = 'color: red; font-size: 13px; background: rgb(255, 255, 219); padding: 1px 5px; border: 1px solid rgba(0, 0, 0, 0.1);';
        console.info('%cDeveloper: ' + options.autor, style);
        console.info('%cFecha: ' + options.date, style);
        console.info('%cEmail: ' + options.email, style);
        console.info('%cFacebook: ' + options.facebook, style);
    }
  
  
    
  }());
